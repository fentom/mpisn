#MINI PROJET 2018-2019 ISN NILS & AUDREY SOUS GNU/GPL -> GNU.ORG
from tkinter import * #Module d'affichage type UI.
import math #Module permettant des d'effectuer des calcule plus complexe.
import webbrowser #Module permettant l'ouverture d'une page WEB l.~188

#LES VARIABLES
largeur_canvas = 600 #défintion de la largeur du canvas.
hauteur_canvas = 600 #défintion de la hauteur du canvas.
compteur=0
colorSeg = "black" #Définition des couleurs par défaults
colorRec = "orange"
colorCer = "cyan"
colorMed = "red"
colorTri = "black"

#LES FONCTIONS
def choisir_point(event ): #Fonction permettant de placer les points sans dépasser le nombre limité de points
    global compteur, coordx1, coordy1, coordx2, coordy2, coordx3, coordy3 #Définie en global les variable pour une utilisation dans le reste du programme
    compteur += 1 #Ajoute 1 à la variable compteur

    if compteur==1: #Vérifie compteur est égale à 1
        coordx1=event.x #Inscrit les coordonné du pointeur en X dans la variable coordx1
        coordy1=event.y #Inscrit les coordonné du pointeur en Y dans la variable coordy1
        print("coordonné de X: " + str(coordx1) + " coordonné de Y: " + str(coordy1)) #Affiche dans la sortie python les coordonné X,Y du point
        canvas.create_line(coordx1-2, coordy1, coordx1+2, coordy1) #Crée une croix pour signaler la présence du point 1.
        canvas.create_line(coordx1, coordy1-2, coordx1, coordy1+2)
    elif compteur==2:
        coordx2=event.x #Inscrit les coordonné du pointeur en X dans la variable coordx2
        coordy2=event.y #Inscrit les coordonné du pointeur en Y dans la variable coordx2
        print("coordonné de X: " + str(coordx2) + " coordonné de Y: " + str(coordy2)) #Affiche dans la sortie python les coordonné X,Y du point
        canvas.create_line(coordx2-2, coordy2, coordx2+2, coordy2) #Crée une croix pour signaler la présence du point 2.
        canvas.create_line(coordx2, coordy2-2, coordx2, coordy2+2)
    elif compteur==3:
        coordx3=event.x #Inscrit les coordonné du pointeur en X dans la variable coordx3
        coordy3=event.y #Inscrit les coordonné du pointeur en Y dans la variable coordx3
        print("coordonné de X: " + str(coordx3) + " coordonné de Y: " + str(coordy3)) #Affiche dans la sortie python les coordonné X,Y du point
        canvas.create_line(coordx3-2, coordy3, coordx3+2, coordy3) #Crée une croix pour signaler la présence du point 3.
        canvas.create_line(coordx3, coordy3-2, coordx3, coordy3+2)
    else:
        print("\n" "Trop de points.") #Si le nombre de point dépasse 3 le programme nous signale qu'il y en à trop.

def segment(): #Fonction qui permet de tracer des segments
    if compteur!=2: #Vérifie si le compteur est bien égale à 2
        print("\n" "Nombre de points incorrect.") #Informe qu'il n'y a pas assez ou trop de points.
    else:
        canvas.create_line(coordx1, coordy1, coordx2, coordy2, fill=colorSeg) #Crée le segment entre les point coordx1, coordy1 et coordx2, coordy2.
        print("Segment: X1=" + str(coordx1) + " Y1=" + str(coordy1) + " X2=" + str(coordx2) + " Y2=" + str(coordy2)) #Affiche dans la sortie python les coordonné du segment

def rectangle(): #Fonction qui permet de tracer des rectangle
    if compteur!=2: #Vérifie si le compteur est bien égale à 2
        print("\n" "Nombre de points incorrect.") #Informe qu'il n'y a pas assez ou trop de points.
    else:
        canvas.create_rectangle(coordx1, coordy1, coordx2, coordy2, outline=colorRec) #Crée un rectangle entre avec les points coordx1, coordy1 et coordx2, coordy2.
        print("Rectangle: X1=" + str(coordx1) + " Y1=" + str(coordy1) + " X2=" + str(coordx2) + " Y2=" + str(coordy2)) #Affiche dans la sortie python les coordonné du rectangle

def cercle(): #Fonction qui permet de tracer des cercle
    if compteur!=2: #Vérifie si le compteur est bien égale à 2
        print("\n" "Nombre de points incorrect.") #Informe qu'il n'y a pas assez ou trop de points.
    else:
        Xm = (coordx1 + coordx2) / 2 #Calcule les coordonné du milieu entre les deux points en X
        Ym = (coordy1 + coordy2) / 2 #Calcule les coordonné du milieu entre les deux points en Y
        r = math.sqrt((coordx1 - coordx2)*(coordx1 - coordx2)+ (coordy1 - coordy2)*(coordy1 - coordy2))/2 #Calcule la moitié de la diagonale du carré
        print("Moitié diagonale carré: " + str(r)) #Affiche dans la sortie python la moitié de la diagonale du carré.
        print("Centre cercle: " + "Xm=" + str(Xm) + " Ym=" + str(Ym)) #Affiche dans la sortie python les coordonné du centre du cercle.

        canvas.create_oval(Xm+r, Ym-r, Xm-r, Ym+r, outline=colorCer) #Crée dans le canvas le cercle.
        print("Cercle: X1=" + str(coordx1) + " Y1=" + str(coordy1) + " X2=" + str(coordx2) + " Y2=" + str(coordy2)) #Affiche dans la sortie python les coordonné du cercle

def mediatrice(): #Fonction qui permet de tracer des médiatrice
    if compteur!=2: #Vérifie si le compteur est bien égale à 2.
        print("\n" "Nombre de points incorrect.") #Informe qu'il n'y a pas assez ou trop de points.
    else:
        canvas.create_line(coordx1, coordy1, coordx2, coordy2) #Crée le segment entre les point coordx1, coordy1 et coordx2, coordy2.
        print("Segment: X1=" + str(coordx1) + " Y1=" + str(coordy1) + " X2=" + str(coordx2) + " Y2=" + str(coordy2)) #Affiche dans la sortie python les coordonné du segment
        Xm = (coordx1 + coordx2) / 2 #Calcule les coordonné du milieu entre les deux points en X
        Ym = (coordy1 + coordy2) / 2 #Calcule les coordonné du milieu entre les deux points en Y
        a = -1/((coordy2-coordy1)/(coordx2-coordx1)) #Coefficient directeur de la médiatrice.

        canvas.create_line(Xm, Ym, coordx2, a*coordx2+Ym-a*Xm,fill=colorMed, dash=(4, 4)) #Crée une médiatrice.
        canvas.create_line(Xm, Ym, coordx1, a*coordx1+Ym-a*Xm,fill=colorMed, dash=(4, 4)) #Crée la seconde médiatrice.

def triangle(): #Fonction qui permet de tracer des triangle
    if compteur!=3: #Vérifie si le compteur est bien égale à 3.
        print("\n" "Nombre de points incorrect.") #Informe qu'il n'y a pas assez ou trop de points.
    else:
            canvas.create_line(coordx1, coordy1, coordx2, coordy2, fill=colorTri) #Crée le segment entre les point coordx1, coordy1 et coordx2, coordy2.
            print("Segment: X1=" + str(coordx1) + " Y1=" + str(coordy1) + " X2=" + str(coordx2) + " Y2=" + str(coordy2)) #Affiche dans la sortie python les coordonné du segment
            canvas.create_line(coordx1, coordy1, coordx3, coordy3, fill=colorTri) #Crée le segment entre les point coordx1, coordy1 et coordx3, coordy3.
            print("Segment: X1=" + str(coordx1) + " Y1=" + str(coordy1) + " X3=" + str(coordx3) + " Y3=" + str(coordy3)) #Affiche dans la sortie python les coordonné du segment
            canvas.create_line(coordx2, coordy2, coordx3, coordy3, fill=colorTri) #Crée le segment entre les point coordx3, coordy3 et coordx2, coordy2.
            print("Segment: X2=" + str(coordx2) + " Y2=" + str(coordy2) + " X3=" + str(coordx3) + " Y3=" + str(coordy3)) #Affiche dans la sortie python les coordonné du segment

def clear(): #Fonction qui permet de réinitialiser le canvas et ses variables
    global compteur, coordx1, coordy1, coordx2, coordy2, coordx3, coordy3 #Définie en global les variable pour une utilisation dans le reste du programme
    compteur=0
    coordx1=0
    coordx2=0
    coordx3=0
    coordy1=0
    coordy2=0
    coordy3=0
    canvas.delete("all")

def colorChoice(): #Fonction qui permet de choisir les couleur des élément afficher dans le canvas.
    global vseg, vrec, vcer, vmed, vtri, colorCer, colorMed, colorRec, colorSeg #Définie en global les variable pour une utilisation dans le reste du programme
    fenColor = Tk() #Crée une nouvelle fenètre
    fenColor.geometry("180x160") #Donne ses dimensions
    fenColor.title("Couleurs") #Donne le titre à cette fenètre
    listeOptions = ('grey', 'black', 'red', 'green', 'blue', 'cyan', 'yellow', 'magenta', 'orange') #Liste de toute les couleurs disponibles

    LabelSegment = Label(fenColor, text="Couleur segment: ") #Ajoute du texte devant le menu option.
    vseg = StringVar() #Définie la variable en string
    OMsegment = OptionMenu(fenColor, vseg, *listeOptions) #Crée le menu d'option
    OMsegment.grid(row = 0, column = 1) #Affiche le label dans la fenètre
    LabelSegment.grid(row = 0, column = 0) #Affiche le menu dans la fenètre
    vseg.trace('w', change_dropdown) #Permet de suivre les instructions Python au fur et à mesure qu'elles sont exécutées.

    Labelrectangle = Label(fenColor, text="Couleur rectangle: ")
    vrec = StringVar()
    OMrectangle = OptionMenu(fenColor, vrec, *listeOptions)
    OMrectangle.grid(row = 1, column = 1)
    Labelrectangle.grid(row = 1, column = 0)
    vrec.trace('w', change_dropdown)

    Labelcercle = Label(fenColor, text="Couleur cercle: ")
    vcer = StringVar()
    OMcercle = OptionMenu(fenColor, vcer, *listeOptions)
    OMcercle.grid(row = 2, column = 1)
    Labelcercle.grid(row = 2, column = 0)
    vcer.trace('w', change_dropdown)

    Labeltriangle = Label(fenColor, text="Couleur triangle: ")
    vtri = StringVar()
    OMtriangle = OptionMenu(fenColor, vtri, *listeOptions)
    OMtriangle.grid(row = 3, column = 1)
    Labeltriangle.grid(row = 3, column = 0)
    vtri.trace('w', change_dropdown)

    Labelmediatrice = Label(fenColor, text="Couleur mediatrice: ")
    vmed = StringVar()
    OMmediatrice = OptionMenu(fenColor, vmed, *listeOptions)
    OMmediatrice.grid(row = 4, column = 1)
    Labelmediatrice.grid(row = 4, column = 0)
    vmed.trace('w', change_dropdown)

    fenColor.mainloop()

def change_dropdown(*args): #Fonction qui ajoute les valeurs choisit par l'utilisateur dans des variables
    global colorCer, colorMed, colorRec, colorSeg, colorTri #Définie en global les variable pour une utilisation dans le reste du programme
    colorSeg = vseg.get() #Ajoute la couleur sélectionné dans la variable choisit.
    colorRec = vrec.get()
    colorCer = vcer.get()
    colorTri = vtri.get()
    colorMed = vmed.get()

def apropos(): #Affiche en fenètre qui donne des indication sur le projet
    fenAbout = Tk() #Crée une fenètre
    fenAbout.geometry("280x80") #Donne ses dimensions
    fenAbout.title("À propos") #Donne le titre à cette fenètre
    text = Label(fenAbout, text='\n' + "Mini projet réalisé en ISN,"+ '\n' +" sur le thème:" + '\n' +"Créer un traceur de forme style Géogébra." + '\n') #Affiche un texte d'explication.
    text.pack() #Affiche le texte dans la fenètre

fen = Tk() #Crée la fenètre principale
fen.title('Mini-Projet') #Donne un titre à cette fenètre

canvas = Canvas(fen, width = largeur_canvas, height = hauteur_canvas, bg='white') #Crée le canvas avec les dimensions du début
canvas.grid(row = 0, column = 0) #Afiche le canavas dans la fenètre

menu = Menu(fen) #Crée un menu en haut de la fenètre
fen.config(menu=menu) #Configure la fenètre
formes = Menu(menu) #Crée un onglet.
menu.add_cascade(label="Formes", menu=formes) #L'ajoute au menu
formes.add_command(label="Segment", command=segment) #Ajoute des boutton dans l'onglet.
formes.add_command(label="Rectangle", command=rectangle)
formes.add_command(label="Triangle", command=triangle)
formes.add_command(label="Cercle", command=cercle)
formes.add_command(label="Médiatrice", command=mediatrice)

option = Menu(menu) #Crée un onglet.
menu.add_cascade(label="Options", menu=option) #L'ajoute au menu
option.add_command(label="Couleurs", command=colorChoice) #Ajoute des boutton dans l'onglet.
option.add_command(label="Réinitialiser", command=clear)
option.add_command(label="Quitter", command=lambda: exit())

about = Menu(menu) #Crée un onglet.
menu.add_cascade(label="Infos", menu=about) #L'ajoute au menu
about.add_command(label="À propos", command=apropos) #Ajoute des boutton dans l'onglet.
about.add_command(label="Code source", command=lambda: webbrowser.open('https://framagit.org/fentom/mpisn/blob/master/mini-projet.py')) #Permet d'ouvrir le code source dans Gitlab

canvas.bind('<Button-1>',choisir_point) #Renvoie à une fonction au clic gauche.

fen.mainloop()
